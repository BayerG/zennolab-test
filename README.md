# Тестовое задание для ZennoLab

## Установка

```shell
python -m venv venv
source venv/bin/activate  # venv\Scripts\activate для Windows
pip install --upgrade pip setuptools  # python -m pip install --upgrade pip setuptools
sh install.sh
```

## Скачивание датасетов

```shell
python download.py
```

## Демонстрация

```shell
python demo.py [--image input.jpg] [--output output.jpg]
```

## Запуск решения (подсчёт метрик)

```shell
python main.py
```
