import argparse
import json
from time import monotonic
from pathlib import Path
import cv2

import numpy as np
import numpy.linalg
from tqdm import tqdm

from ml import predict
from mmpose.apis import MMPoseInferencer

start_time = monotonic()
inferencer = MMPoseInferencer('animal')
json_filenames = list(Path().glob('tasks/**/*.json'))
distances = []

for json_filename in tqdm(json_filenames):
    try:
        with open(json_filename) as file:
            data = json.load(file)[0]
    except (json.JSONDecodeError, IndexError):
        continue

    image_filename = (json_filename.parent / json_filename.stem).with_suffix('.jpg')
    image = cv2.imread(str(image_filename))

    y_true = np.array([data['x'], data['y']])
    y_pred = predict(image, inferencer)
    distances.append(np.linalg.norm(y_pred - y_true))


distances = np.array(distances)
mean_distance = np.mean(distances)
mean_accuracy = np.mean(distances < 0.1)
total_time = monotonic() - start_time

print(f'Средняя точность: {mean_accuracy}\nСреднее расстояние: {mean_distance}\nОбщее время: {total_time} сек.')
