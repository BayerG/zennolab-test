import gdown
from py7zr import unpack_7zarchive
import shutil

print('Downloading...')
gdown.download(id='1UpPoMB2Ke91xX8oUrKicFlDp-7sZAkbo')
print('Unpacking...')
shutil.register_unpack_format('7zip', ['.7z'], unpack_7zarchive)
shutil.unpack_archive('tasks.7z', '.')
