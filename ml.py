import numpy as np

LEFT_EYE = 0
RIGHT_EYE = 1


def predict(image, inferencer):
    image_size = np.array(image.shape[:2][::-1])
    keypoints = np.array(next(inferencer(image, show=False))['predictions'][0][0]['keypoints']) / image_size
    return np.mean([keypoints[LEFT_EYE], keypoints[RIGHT_EYE]], axis=0)
