import argparse

import cv2
from matplotlib import pyplot as plt

from ml import predict
from mmpose.apis import MMPoseInferencer

parser = argparse.ArgumentParser()
parser.add_argument('--image', default='tasks/squirrels_head/00000beee3cbe72f.jpg')
parser.add_argument('--output', default='')
args = parser.parse_args()

inferencer = MMPoseInferencer('animal')
image = cv2.imread(args.image)
x, y = (predict(image, inferencer) * image.shape[:2][::-1]).astype('int')

cv2.circle(image, (x, y), 2, (0, 0, 255), 2)
if not args.output:
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.show()
else:
    cv2.imwrite(args.output, image)
